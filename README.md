# Centralized Monetary Machine (Server)

A server implementation of an ATM, that allows dispensing notes of 20$ and 50$

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Java8
Apache Maven 3.5.3

```

### Installing

A step by step series of examples that tell you how to get a development env running

```
mvn clean install -DskipTests
```

## Running the tests

```
mvn test
```

## Deployment

After running ```mvn clean install -DskipTests``` an executable jar file is created in target directory.

```
cd target
java -jar centralized-monetary-machine-1.0.0-SNAPSHOT.jar
```

## Built With

* [SpringBoot](https://spring.io/projects/spring-boot) - The Java framework used
* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Lorik Hajrullai** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
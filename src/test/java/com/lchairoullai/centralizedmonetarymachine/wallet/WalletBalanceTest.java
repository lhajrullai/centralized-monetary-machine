package com.lchairoullai.centralizedmonetarymachine.wallet;

import com.lchairoullai.centralizedmonetarymachine.config.NoteType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class WalletBalanceTest {

    private WalletBalance walletBalance;

    @Before
    public void setUp() throws Exception {
        walletBalance = WalletBalance.getInstance();
    }

    @Test
    public void checkInstance() throws Exception {
        assertTrue(walletBalance != null);
    }

    @Test
    public void isInsufficient() throws Exception {
        walletBalance.initializeAmount(null);
        assertTrue(walletBalance.isInsufficient(1d));
    }

    @Test
    public void reduceAmount() throws Exception {

        Map<NoteType, Integer> initialNotes = new HashMap<>();
        initialNotes.put(NoteType.TWENTY, 10);
        initialNotes.put(NoteType.FIFTY, 10);
        // 200 + 500 = 700
        walletBalance.initializeAmount(initialNotes);

        Map<NoteType, Integer> notes = new HashMap<>();
        notes.put(NoteType.FIFTY, 1);
        notes.put(NoteType.TWENTY, 1);

        assertFalse(walletBalance.isInsufficient(70d));
        walletBalance.reduceAmount(notes);

        Map<NoteType, Integer> notes1 = walletBalance.getNotes();
        assertTrue(notes1.get(NoteType.FIFTY) == 9);
        assertTrue(notes1.get(NoteType.TWENTY) == 9);
    }

    @Test
    public void reduceAmountGreaterThanAvailable() throws Exception {
        Map<NoteType, Integer> initialNotes = new HashMap<>();
        initialNotes.put(NoteType.TWENTY, 10);
        initialNotes.put(NoteType.FIFTY, 10);
        // 200 + 500 = 700
        walletBalance.initializeAmount(initialNotes);

        Map<NoteType, Integer> notes = new HashMap<>();
        notes.put(NoteType.FIFTY, 11);
        notes.put(NoteType.TWENTY, 11);

        walletBalance.reduceAmount(notes);

        Map<NoteType, Integer> notes1 = walletBalance.getNotes();
        assertTrue(notes1.get(NoteType.FIFTY) == 10);
        assertTrue(notes1.get(NoteType.TWENTY) == 10);

    }


    @Test
    public void getNotes() throws Exception {

        Map<NoteType, Integer> initialNotes = new HashMap<>();
        initialNotes.put(NoteType.TWENTY, 10);
        initialNotes.put(NoteType.FIFTY, 10);
        // 200 + 500 = 700
        walletBalance.initializeAmount(initialNotes);

        Map<NoteType, Integer> notes = walletBalance.getNotes();
        assertTrue(notes.get(NoteType.FIFTY) == 10);
        assertTrue(notes.get(NoteType.TWENTY) == 10);

    }
}
package com.lchairoullai.centralizedmonetarymachine.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class NoteTypeTest {

    @Test
    public void should() throws Exception {
        assertTrue(NoteType.FIFTY.getValue() == 50);
        assertTrue(NoteType.TWENTY.getValue() == 20);
        assertTrue(NoteType.FIFTY.getValue() != NoteType.TWENTY.getValue());
    }

    @Test
    public void fromValueReturnCorrectNoteType() throws Exception {
        assertTrue(NoteType.fromValue(20d).equals(NoteType.TWENTY));
        assertTrue(NoteType.fromValue(50d).equals(NoteType.FIFTY));
    }
}
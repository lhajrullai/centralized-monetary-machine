package com.lchairoullai.centralizedmonetarymachine.service.util;

import com.lchairoullai.centralizedmonetarymachine.config.NoteType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class UtilityTest {

    @Test
    public void shouldFindAtLeastOneCombination() {
        List<Map<NoteType, Integer>> result = new ArrayList<>();
        Utility.calculatePossibleCombinations(result, 120d);
        assertTrue(!result.isEmpty());
    }

    @Test
    public void shouldFindTwoCombinations() throws Exception {
        List<Map<NoteType, Integer>> result = new ArrayList<>();
        Utility.calculatePossibleCombinations(result, 120d);
        assertTrue(result.size() == 2);
    }

    @Test
    public void shouldFindCorrectCombinations() throws Exception {
        List<Map<NoteType, Integer>> result = new ArrayList<>();
        List<Map<NoteType, Integer>> initial = new ArrayList<>();

        Map<NoteType, Integer> noteTypeIntegerMap = new HashMap<>();
        noteTypeIntegerMap.put(NoteType.TWENTY, 6);
        noteTypeIntegerMap.put(NoteType.FIFTY, 0);

        Map<NoteType, Integer> noteTypeIntegerMap2 = new HashMap<>();
        noteTypeIntegerMap2.put(NoteType.TWENTY, 1);
        noteTypeIntegerMap2.put(NoteType.FIFTY, 2);

        initial.add(noteTypeIntegerMap);
        initial.add(noteTypeIntegerMap2);

        Utility.calculatePossibleCombinations(result, 120d);
        assertTrue(new HashSet<>(result).equals(new HashSet<>(initial)));
    }

    @Test
    public void shouldNotFindAnyCombination() {
        List<Map<NoteType, Integer>> result = new ArrayList<>();
        Utility.calculatePossibleCombinations(result, 30d);
        assertTrue(result.isEmpty());
    }

}
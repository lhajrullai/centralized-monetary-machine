package com.lchairoullai.centralizedmonetarymachine.service;

import com.lchairoullai.centralizedmonetarymachine.config.NoteType;
import com.lchairoullai.centralizedmonetarymachine.service.dto.FundDTO;
import com.lchairoullai.centralizedmonetarymachine.wallet.WalletBalance;
import com.lchairoullai.centralizedmonetarymachine.web.rest.errors.CombinationNotAvailable;
import com.lchairoullai.centralizedmonetarymachine.web.rest.errors.InsufficientFunds;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class WithdrawalServiceTest {

    private WalletBalance walletBalance;
    private WithdrawalService withdrawalService;

    @Before
    public void setUp() throws Exception {
        walletBalance = WalletBalance.getInstance();

        Map<NoteType, Integer> notes = new HashMap<>();
        notes.put(NoteType.TWENTY, 10);
        notes.put(NoteType.FIFTY, 10);
        // 200 + 500 = 700
        walletBalance.initializeAmount(notes);
        withdrawalService = new WithdrawalService(walletBalance);
    }

    @Test(expected = InsufficientFunds.class)
    public void withdrawInsufficientException() throws Exception {
        Map<NoteType, Integer> notes = new HashMap<>();
        notes.put(NoteType.TWENTY, 10);
        notes.put(NoteType.FIFTY, 10);
        // 200 + 500 = 700
        walletBalance.initializeAmount(notes);
        withdrawalService.withdraw(800d);
    }

    @Test(expected = CombinationNotAvailable.class)
    public void withdrawCombinationNotAvailableException() throws Exception {
        Map<NoteType, Integer> notes = new HashMap<>();
        notes.put(NoteType.TWENTY, 10);
        notes.put(NoteType.FIFTY, 10);
        // 200 + 500 = 700
        walletBalance.initializeAmount(notes);
        withdrawalService.withdraw(125d);
    }

    @Test
    public void minWithdrawalAmountShouldBe20() throws Exception {
        assertTrue(withdrawalService.getMinimumWithdrawalAmount() == 20d);
    }


    @Test
    public void withdrawalReturnsCorrectQuantitiesOfNotes() throws Exception {
        Map<NoteType, Integer> notes = new HashMap<>();
        notes.put(NoteType.TWENTY, 10);
        notes.put(NoteType.FIFTY, 10);
        // 200 + 500 = 700
        walletBalance.initializeAmount(notes);

        FundDTO withdraw = withdrawalService.withdraw(120d);
        assertTrue(withdraw.getAmount() == 120d);
        assertTrue(withdraw.getNotes().get(NoteType.TWENTY) == 1);
        assertTrue(withdraw.getNotes().get(NoteType.FIFTY) == 2);

        assertFalse(withdraw.getNotes().get(NoteType.TWENTY) == 6);
        assertFalse(withdraw.getNotes().get(NoteType.FIFTY) == 0);
    }

}
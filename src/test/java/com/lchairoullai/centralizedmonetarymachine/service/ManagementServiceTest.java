package com.lchairoullai.centralizedmonetarymachine.service;

import com.lchairoullai.centralizedmonetarymachine.config.NoteType;
import com.lchairoullai.centralizedmonetarymachine.service.dto.FundDTO;
import com.lchairoullai.centralizedmonetarymachine.wallet.WalletBalance;
import com.lchairoullai.centralizedmonetarymachine.web.rest.errors.AlreadyInitialized;
import com.lchairoullai.centralizedmonetarymachine.web.rest.errors.NotInitialized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class ManagementServiceTest {

    ManagementService managementService;
    WalletBalance walletBalance;

    @Before
    public void setUp() throws Exception {
        walletBalance = WalletBalance.getInstance();
        managementService = new ManagementService(walletBalance);
    }


    @Test(expected = NotInitialized.class)
    public void getFundsShouldThrowNotInitialized() throws Exception {
        walletBalance.initializeAmount(null);
        managementService.getFunds();
    }


    @Test(expected = NotInitialized.class)
    public void resetFundsShouldThrowNotInitialized() throws Exception {

        Map<NoteType, Integer> resetNotes = new HashMap<>();
        resetNotes.put(NoteType.TWENTY, 12);
        resetNotes.put(NoteType.FIFTY, 12);

        walletBalance.initializeAmount(null);
        managementService.resetFunds(resetNotes);
    }

    @Test
    public void initializeFunds() throws Exception {
        Map<NoteType, Integer> initialNotes = new HashMap<>();
        initialNotes.put(NoteType.TWENTY, 10);
        initialNotes.put(NoteType.FIFTY, 10);


        walletBalance.initializeAmount(null);
        FundDTO fundDTO = managementService.initializeFunds(initialNotes);

        FundDTO expected = new FundDTO();
        expected.setCurrency("USD");
        expected.setNotes(initialNotes);
        expected.setAmount(700d);
        assertTrue(fundDTO.equals(expected));
    }

    @Test(expected = AlreadyInitialized.class)
    public void initializeFundsShouldThrowAlreadyInitialized() throws Exception {
        Map<NoteType, Integer> initialNotes = new HashMap<>();
        initialNotes.put(NoteType.TWENTY, 10);
        initialNotes.put(NoteType.FIFTY, 10);

        walletBalance.initializeAmount(initialNotes);
        managementService.initializeFunds(initialNotes);
    }

    @Test
    public void resetFunds() throws Exception {

        Map<NoteType, Integer> resetNotes = new HashMap<>();
        resetNotes.put(NoteType.TWENTY, 12);
        resetNotes.put(NoteType.FIFTY, 12);

        walletBalance.initializeAmount(resetNotes);
        FundDTO fundDTO = managementService.resetFunds(resetNotes);

        FundDTO expected = new FundDTO();
        expected.setCurrency("USD");
        expected.setNotes(resetNotes);
        expected.setAmount(840d);
        assertTrue(fundDTO.equals(expected));

    }

    @Test
    public void getFunds() throws Exception {

        Map<NoteType, Integer> resetNotes = new HashMap<>();
        resetNotes.put(NoteType.TWENTY, 12);
        resetNotes.put(NoteType.FIFTY, 12);

        walletBalance.initializeAmount(resetNotes);


        Map<NoteType, Integer> notes = new HashMap<>();
        notes.put(NoteType.TWENTY, 12);
        notes.put(NoteType.FIFTY, 12);

        FundDTO expected = new FundDTO();
        expected.setCurrency("USD");
        expected.setNotes(notes);
        expected.setAmount(840d);

        assertTrue(managementService.getFunds().equals(expected));
    }

}
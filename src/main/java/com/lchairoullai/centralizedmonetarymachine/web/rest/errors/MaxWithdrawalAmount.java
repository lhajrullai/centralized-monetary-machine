package com.lchairoullai.centralizedmonetarymachine.web.rest.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MaxWithdrawalAmount extends RuntimeException {

    public MaxWithdrawalAmount() {
        super("The amount requested is less than minimum withdrawal amount");
    }

    public MaxWithdrawalAmount(String message, Throwable cause) {
        super(message, cause);
    }

    public MaxWithdrawalAmount(String message) {
        super(message);
    }

    public MaxWithdrawalAmount(Throwable cause) {
        super(cause);
    }

}

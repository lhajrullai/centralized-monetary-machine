package com.lchairoullai.centralizedmonetarymachine.web.rest;

import com.lchairoullai.centralizedmonetarymachine.config.Constants;
import com.lchairoullai.centralizedmonetarymachine.service.WithdrawalService;
import com.lchairoullai.centralizedmonetarymachine.service.dto.FundDTO;
import com.lchairoullai.centralizedmonetarymachine.web.rest.errors.MaxWithdrawalAmount;
import com.lchairoullai.centralizedmonetarymachine.web.rest.errors.MinWithdrawalAmount;
import com.lchairoullai.centralizedmonetarymachine.web.rest.vm.WithdrawAmount;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class WithdrawalController {

    private final WithdrawalService withdrawalService;

    public WithdrawalController(WithdrawalService withdrawalService) {
        this.withdrawalService = withdrawalService;
    }

    @PostMapping("/withdraw")
    public ResponseEntity<FundDTO> withdraw(@RequestBody @Valid WithdrawAmount withdrawAmount) {
        double minimumWithdrawalAmount = withdrawalService.getMinimumWithdrawalAmount();
        if (withdrawAmount.getAmount() < minimumWithdrawalAmount) {
            throw new MinWithdrawalAmount("Minimum withdrawal amount is " + minimumWithdrawalAmount + "$");
        }

        if (withdrawAmount.getAmount() > Constants.MAX_WITHDRAWAL_AMOUNT) {
            throw new MaxWithdrawalAmount("Maximum withdrawal amount is " + Constants.MAX_WITHDRAWAL_AMOUNT + "$");
        }

        return ResponseEntity.ok(withdrawalService.withdraw(withdrawAmount.getAmount()));
    }

}

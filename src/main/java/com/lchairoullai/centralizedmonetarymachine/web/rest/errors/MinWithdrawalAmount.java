package com.lchairoullai.centralizedmonetarymachine.web.rest.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MinWithdrawalAmount extends RuntimeException {

    public MinWithdrawalAmount() {
        super("The amount requested is less than minimum withdrawal amount");
    }

    public MinWithdrawalAmount(String message, Throwable cause) {
        super(message, cause);
    }

    public MinWithdrawalAmount(String message) {
        super(message);
    }

    public MinWithdrawalAmount(Throwable cause) {
        super(cause);
    }

}

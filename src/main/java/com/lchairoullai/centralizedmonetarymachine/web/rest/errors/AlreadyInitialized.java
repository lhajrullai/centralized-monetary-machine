package com.lchairoullai.centralizedmonetarymachine.web.rest.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_REQUIRED)
public class AlreadyInitialized extends RuntimeException {

    public AlreadyInitialized() {
        super("Already Initialized");
    }

    public AlreadyInitialized(String message, Throwable cause) {
        super(message, cause);
    }

    public AlreadyInitialized(String message) {
        super(message);
    }

    public AlreadyInitialized(Throwable cause) {
        super(cause);
    }

}

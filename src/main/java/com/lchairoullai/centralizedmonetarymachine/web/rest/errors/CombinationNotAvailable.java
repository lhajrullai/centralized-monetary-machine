package com.lchairoullai.centralizedmonetarymachine.web.rest.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CombinationNotAvailable extends RuntimeException {

    public CombinationNotAvailable() {
        super("Cannot withdraw amount");
    }

    public CombinationNotAvailable(String message, Throwable cause) {
        super(message, cause);
    }

    public CombinationNotAvailable(String message) {
        super(message);
    }

    public CombinationNotAvailable(Throwable cause) {
        super(cause);
    }
}

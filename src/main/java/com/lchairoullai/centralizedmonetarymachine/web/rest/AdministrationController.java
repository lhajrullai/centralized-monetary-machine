package com.lchairoullai.centralizedmonetarymachine.web.rest;

import com.lchairoullai.centralizedmonetarymachine.config.NoteType;
import com.lchairoullai.centralizedmonetarymachine.service.ManagementService;
import com.lchairoullai.centralizedmonetarymachine.service.dto.FundDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/management")
public class AdministrationController {

    private final ManagementService managementService;

    public AdministrationController(ManagementService managementService) {
        this.managementService = managementService;
    }

    @PostMapping("/funds")
    public ResponseEntity<FundDTO> initializeMachine(@RequestBody Map<NoteType, Integer> notes) {
        return ResponseEntity.ok(managementService.initializeFunds(notes));
    }

    @PutMapping("/funds")
    public ResponseEntity<FundDTO> resetMachine(@RequestBody Map<NoteType, Integer> notes) {
        return ResponseEntity.ok(managementService.resetFunds(notes));
    }

    @GetMapping("/funds")
    public ResponseEntity<FundDTO> getFunds() {
        return ResponseEntity.ok(managementService.getFunds());
    }

}

package com.lchairoullai.centralizedmonetarymachine.web.rest.vm;
import javax.validation.constraints.NotNull;

public class WithdrawAmount {

    @NotNull
    private Double amount;

    public WithdrawAmount() {
    }

    public WithdrawAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "WithdrawAmount{" +
                "amount=" + amount +
                '}';
    }
}

package com.lchairoullai.centralizedmonetarymachine.web.rest.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_REQUIRED)
public class InsufficientFunds extends RuntimeException {

    public InsufficientFunds() {
        super("Insufficient Funds");
    }

    public InsufficientFunds(String message, Throwable cause) {
        super(message, cause);
    }

    public InsufficientFunds(String message) {
        super(message);
    }

    public InsufficientFunds(Throwable cause) {
        super(cause);
    }

}
package com.lchairoullai.centralizedmonetarymachine.web.rest.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_REQUIRED)
public class NotInitialized extends RuntimeException {

    public NotInitialized() {
        super("Machine not initialized");
    }

    public NotInitialized(String message, Throwable cause) {
        super(message, cause);
    }

    public NotInitialized(String message) {
        super(message);
    }

    public NotInitialized(Throwable cause) {
        super(cause);
    }

}

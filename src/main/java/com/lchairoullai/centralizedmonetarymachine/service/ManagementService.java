package com.lchairoullai.centralizedmonetarymachine.service;

import com.lchairoullai.centralizedmonetarymachine.config.NoteType;
import com.lchairoullai.centralizedmonetarymachine.service.dto.FundDTO;
import com.lchairoullai.centralizedmonetarymachine.wallet.WalletBalance;
import com.lchairoullai.centralizedmonetarymachine.web.rest.errors.AlreadyInitialized;
import com.lchairoullai.centralizedmonetarymachine.web.rest.errors.NotInitialized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ManagementService {

    private WalletBalance walletBalance;

    @Autowired
    public ManagementService(final WalletBalance walletBalance) {
        this.walletBalance = walletBalance;
    }

    public FundDTO initializeFunds(Map<NoteType, Integer> notes) {

        synchronized (this) {
            if (walletBalance.getNotes() != null) {
                throw new AlreadyInitialized();
            }
            walletBalance.initializeAmount(notes);
            return new FundDTO(notes);
        }

    }

    public FundDTO resetFunds(Map<NoteType, Integer> notes) {

        synchronized (this) {

            if (walletBalance.getNotes() == null) {
                throw new NotInitialized();
            }

            walletBalance.initializeAmount(notes);
            return new FundDTO(notes);
        }

    }

    public FundDTO getFunds() {
        synchronized (this) {
            if (walletBalance.getNotes() == null) {
                throw new NotInitialized();
            }

            return new FundDTO(walletBalance.getNotes());
        }
    }
}

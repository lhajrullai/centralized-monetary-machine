package com.lchairoullai.centralizedmonetarymachine.service;

import com.lchairoullai.centralizedmonetarymachine.config.NoteType;
import com.lchairoullai.centralizedmonetarymachine.service.dto.FundDTO;
import com.lchairoullai.centralizedmonetarymachine.service.util.Utility;
import com.lchairoullai.centralizedmonetarymachine.wallet.WalletBalance;
import com.lchairoullai.centralizedmonetarymachine.web.rest.errors.CombinationNotAvailable;
import com.lchairoullai.centralizedmonetarymachine.web.rest.errors.InsufficientFunds;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

@Service
public class WithdrawalService {

    private final WalletBalance walletBalance;

    WithdrawalService(WalletBalance walletBalance) {
        this.walletBalance = walletBalance;
    }

    public FundDTO withdraw(Double amountToWithdraw) {
        synchronized (this) {
            if (walletBalance.isInsufficient(amountToWithdraw)) {
                throw new InsufficientFunds();
            }

            Optional<Map<NoteType, Integer>> combination = getCombination(amountToWithdraw);
            if (!combination.isPresent()) {
                throw new CombinationNotAvailable("Could not withdraw notes for " + amountToWithdraw + " $");
            }

            walletBalance.reduceAmount(combination.get());
            return new FundDTO(combination.get());
        }
    }

    public double getMinimumWithdrawalAmount() {
        return Stream.of(NoteType.values())
                .map(NoteType::getValue)
                .mapToDouble(Double::doubleValue)
                .min()
                .orElse(0d);
    }

    private Optional<Map<NoteType, Integer>> getCombination(Double amountToWithdraw) {

        List<Map<NoteType, Integer>> result = new ArrayList<>();
        Utility.calculatePossibleCombinations(result, amountToWithdraw);

        Collections.reverse(result);

        Map<NoteType, Integer> noteTypeIntegerMap = result.stream()
                .filter(combination -> !combinationNotAvailable(combination))
                .findFirst()
                .orElse(null);

        return Optional.ofNullable(noteTypeIntegerMap);
    }

    private boolean combinationNotAvailable(Map<NoteType, Integer> combination) {
        return combination.entrySet()
                .stream()
                .anyMatch(comb -> walletBalance.getNotes().get(comb.getKey()) < comb.getValue());
    }

}
package com.lchairoullai.centralizedmonetarymachine.service.util;

import com.lchairoullai.centralizedmonetarymachine.config.NoteType;

import java.util.*;

public final class Utility {

    public static void calculatePossibleCombinations(List<Map<NoteType, Integer>> result, Double amountToWithdraw) {
        List<NoteType> notes = new ArrayList<>();
        calculatePossibleCombinations(result, notes, 0d, 0d, amountToWithdraw);
    }

    /*
    * This method uses brute force to calculate all possible combinations of notes that equal to the desired withdrawal amount
    * */
    private static void calculatePossibleCombinations(List<Map<NoteType, Integer>> result, List<NoteType> notes, Double highest, Double sum, Double amountToWithdraw) {

        if (Objects.equals(sum, amountToWithdraw)) {
            result.add(construct(notes));
            return;
        }

        if (sum > amountToWithdraw) {
            return;
        }

        EnumSet.allOf(NoteType.class).forEach(amount -> {
            if (amount.getValue() >= highest) {
                List<NoteType> copy = new ArrayList<>(notes);
                copy.add(amount);
                calculatePossibleCombinations(result, copy, amount.getValue(), sum + amount.getValue(), amountToWithdraw);
            }
        });

    }

    private static Map<NoteType, Integer> construct(List<NoteType> notes) {
        Map<NoteType, Integer> tmp = new HashMap<>();
        EnumSet.allOf(NoteType.class).forEach(noteType -> tmp.put(noteType, Collections.frequency(notes, noteType)));
        return tmp;
    }

}

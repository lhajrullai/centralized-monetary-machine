package com.lchairoullai.centralizedmonetarymachine.service.dto;

import com.lchairoullai.centralizedmonetarymachine.config.CurrencyType;
import com.lchairoullai.centralizedmonetarymachine.config.NoteType;

import java.util.Map;

public class FundDTO {

    private Double amount;
    private String currency;
    private Map<NoteType, Integer> notes;

    public FundDTO() {
    }

    public FundDTO(Map<NoteType, Integer> notes) {
        this.amount = notes.entrySet()
                .stream()
                .map(noteTypeIntegerEntry -> noteTypeIntegerEntry.getKey().getValue() * noteTypeIntegerEntry.getValue())
                .mapToDouble(Double::doubleValue)
                .sum();
        this.currency = CurrencyType.DOLLARS.getValue();
        this.notes = notes;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Map<NoteType, Integer> getNotes() {
        return notes;
    }

    public void setNotes(Map<NoteType, Integer> notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FundDTO fundDTO = (FundDTO) o;

        if (amount != null ? !amount.equals(fundDTO.amount) : fundDTO.amount != null) return false;
        if (currency != null ? !currency.equals(fundDTO.currency) : fundDTO.currency != null) return false;
        return notes != null ? notes.equals(fundDTO.notes) : fundDTO.notes == null;
    }

    @Override
    public String toString() {
        return "FundDTO{" +
                "amount=" + amount +
                ", currency='" + currency + '\'' +
                ", notes=" + notes +
                '}';
    }
}

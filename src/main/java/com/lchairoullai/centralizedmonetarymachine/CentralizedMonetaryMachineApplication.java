package com.lchairoullai.centralizedmonetarymachine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.lchairoullai")
public class CentralizedMonetaryMachineApplication {

    public static void main(String[] args) {
        SpringApplication.run(CentralizedMonetaryMachineApplication.class, args);
    }
}

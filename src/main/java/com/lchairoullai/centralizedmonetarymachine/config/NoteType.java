package com.lchairoullai.centralizedmonetarymachine.config;

public enum NoteType {
    TWENTY(20d),
    FIFTY(50d);

    private double value;

    public double getValue() {
        return this.value;
    }

    NoteType(double value) {
        this.value = value;
    }

    public static NoteType fromValue(Double value) {
        for (NoteType n : NoteType.values()) {
            if (n.getValue() == value) {
                return n;
            }
        }
        throw new IllegalArgumentException();
    }

}

package com.lchairoullai.centralizedmonetarymachine.config;

public enum CurrencyType {
    DOLLARS("USD");

    private String value;

    public String getValue() {
        return this.value;
    }

    CurrencyType(String value) {
        this.value = value;
    }

    public static CurrencyType fromValue(String value) {
        for (CurrencyType n : CurrencyType.values()) {
            if (n.getValue().equals(value)) {
                return n;
            }
        }
        throw new IllegalArgumentException();
    }
}

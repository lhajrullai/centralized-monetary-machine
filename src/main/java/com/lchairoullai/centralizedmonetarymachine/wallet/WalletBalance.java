package com.lchairoullai.centralizedmonetarymachine.wallet;

import com.lchairoullai.centralizedmonetarymachine.config.NoteType;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class WalletBalance {

    private static WalletBalance instance;
    private Map<NoteType, Integer> notes;

    private WalletBalance() {
        this.notes = null;
    }

    public static synchronized WalletBalance getInstance() {
        if (WalletBalance.instance == null) {
            WalletBalance.instance = new WalletBalance();
        }

        return WalletBalance.instance;
    }

    public void initializeAmount(Map<NoteType, Integer> notes) {
        this.notes = notes;
    }

    public Map<NoteType, Integer> getNotes() {
        return this.notes;
    }

    public void reduceAmount(Map<NoteType, Integer> notes) {

        boolean b = notes.entrySet()
                .stream()
                .anyMatch(note -> this.getNotes().get(note.getKey()) < note.getValue());

        if (!b) {
            notes.forEach((key, value) -> {
                Integer availableNotes = this.notes.get(key);
                Integer newAvailableNotes = availableNotes - value;
                this.notes.put(key, newAvailableNotes);
            });
        }

    }

    private Double calculateTotalAmount() {
        return this.notes.entrySet()
                .stream()
                .map(noteTypeIntegerEntry -> noteTypeIntegerEntry.getKey().getValue() * noteTypeIntegerEntry.getValue())
                .mapToDouble(Double::doubleValue)
                .sum();
    }

    public boolean isInsufficient(Double amount) {
        return this.notes == null || this.notes.isEmpty() || calculateTotalAmount() < amount;
    }

}
